npm install

mkdir db1 db2 db3

mongod --dbpath db1 --port 27021 --replSet mySet
mongod --dbpath db2 --port 27022 --replSet mySet
mongod --dbpath db3 --port 27023 --replSet mySet

mongo --port 27021
rs.initiate()
rs.add("Diegos-MacBook-Air.local:27022")
rs.add("Diegos-MacBook-Air.local:27023")

mongo --port 27022
mongo --port 27023

db.test.insert({name: 'Diego Mello'})
rs.status()
rs.slaveOk()

export MONGO_URL="mongodb://Diegos-MacBook-Air.local:27021,Diegos-MacBook-Air.local:27022,Diegos-MacBook-Air.local:27023/meteor?replicaSet=mySet&readPreference=primary"

meteor -p3000
meteor -p3001
meteor -p3002

/usr/local/etc/nginx/nginx.conf