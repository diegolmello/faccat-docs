Documents = new Meteor.Collection('documents');

Deps.autorun(function () {
  Meteor.subscribe('documents');
});

Template.root.currentDocumentId = function () {
  return Session.get('currentDocumentId') != '' && Session.get('currentDocumentId') !== undefined;
};

currentDocumentId = function () {
  return Session.get('currentDocumentId');
}

saveDocument = function () {
  var properties = {
    title: $('#doc-title').val(),
    content: $('#doc-content').val(),
    lastSavedAt: new Date()
  };
  
  Documents.update(currentDocumentId(), {$set: properties}, function(error, document) {
    if (error) { console.log(error.reason); }
  });
}

// Document Template

Template.document.events({
  'keyup #doc-title, keyup #doc-content' : saveDocument,
  'click a.close' : function () {
    Session.set('currentDocumentId', '');
  }
});

Template.document.helpers({ 
  currentDocument: function () {
    var doc = Documents.findOne( {_id: currentDocumentId()} );

    if (doc) {
      return doc;
    }
  }
});

// List Template

Template.list.documents = function () {
  return Documents.find({});
};

Template.list.events( {
  'click a.btn': function (event) {
    var documentId = Documents.insert(
      {
        title: 'New document',
        content: '',
        lastSavedAt: new Date()
      }
    );

    Session.set('currentDocumentId', documentId);
  }
});

// Document Row Template

Template.documentRow.events({
  'click tr' : function (event) {
    Session.set('currentDocumentId', event.currentTarget.className);
  }
});
Template.documentRow.helpers({
  'timeago': function(timestamp) {
    return moment(timestamp).fromNow();
  }
});